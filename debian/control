Source: texstudio
Section: editors
Priority: optional
Maintainer: Tom Jampen <tom@cryptography.ch>
Build-Depends:
 adwaita-qt, debhelper-compat (= 13), libhunspell-dev (>= 1.5.1),
 libphonon4qt5-dev, libpoppler-cpp-dev, libpoppler-qt5-dev, libqt5svg5-dev,
 libquazip5-dev, libqtermwidget5-0-dev, pkg-config, qt5-qmake, qtdeclarative5-dev,
 qtscript5-dev, qttools5-dev, zlib1g-dev
Standards-Version: 4.6.1
Homepage: https://texstudio.org/

Package: texstudio
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, libqt5svg5
Recommends:
 adwaita-qt, texlive-base, texlive-latex-base, texlive-latex-recommended,
 texstudio-doc, texstudio-l10n
Suggests:
 hunspell-dictionary, mythes-thesaurus, texlive-fonts-recommended,
 texlive-latex-extra
Description: LaTeX Editor
 TeXstudio is a program based on Texmaker, which integrates many tools needed
 to develop documents with LaTeX in just one application. Using its editor you
 can write your documents with the help of interactive spell checking, syntax
 highlighting, code completion and more...

Package: texstudio-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Replaces: texstudio (<< 2.7.0+debian-2)
Breaks: texstudio (<< 2.7.0+debian-2)
Description: LaTeX Editor (doc)
 TeXstudio is a program based on Texmaker, which integrates many tools needed
 to develop documents with LaTeX in just one application. Using its editor you
 can write your documents with the help of interactive spell checking, syntax
 highlighting, code completion and more...
 .
 This package contains the html documentation and the LaTeX templates.

Package: texstudio-l10n
Section: localization
Architecture: all
Depends: ${misc:Depends}, texstudio (>= ${source:Version})
Replaces: texstudio (<< 2.7.0+debian-2)
Breaks: texstudio (<< 2.7.0+debian-2)
Description: LaTeX Editor (localization)
 TeXstudio is a program based on Texmaker, which integrates many tools needed
 to develop documents with LaTeX in just one application. Using its editor you
 can write your documents with the help of interactive spell checking, syntax
 highlighting, code completion and more...
 .
 This package contains the following translations: ar, br, ca, cs, de, el, es,
 fa, fr, hu, id_ID, ie, it, ja, ko, ko_KR, nb_NO, nl, pl, pt, pt_BR, ru, ru_RU,
 si, sk, sv, tr_TR, ug, uk, vi, zh_CN.
